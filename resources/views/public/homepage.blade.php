<x-fronted.layout.master>

<div class="page-content">
                <section class="intro-section">
                    <div class="owl-carousel owl-theme row owl-nav-bg owl-dot-inner owl-dot-white owl-nav-fade intro-slider animation-slider cols-1 gutter-no"
                        data-owl-options="{
                        'nav': false,
                        'dots': true,
                        'loop': true,
                        'items': 1,
                        
                        'responsive': {
                            '992': {
                                'nav': true,
                                'dots': false
                            }
                        }
                    }">
                        <div class="banner banner-fixed intro-slide1" style="background-color: #edecec">
                            <figure>
                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/slides/1.jpg" alt="intro-banner" width="1903"
                                    height="514" />
                            </figure>
                            <div class="container">
                                <div class="banner-content y-50">
                                    <div class="slide-animate" data-animation-options="{
                                        'name': 'fadeInLeftShorter',
                                        'duration': '1.2s',
                                        'delay': '.2s'
                                    }">
                                        <h4 class="banner-subtitle font-weight-normal text-dark">Lifestyle Collection
                                        </h4>
                                        <h2 class="banner-title slide-animate text-uppercase" data-animation-options="{
                                            'name': 'fadeInLeftShorter',
                                            'duration': '1.6s',
                                            'delay': '.1s'
                                        }">For traveling</h2>
                                        <h3 class="banner-deco slide-animate" data-animation-options="{
                                            'name': 'fadeInLeftShorter',
                                            'duration': '1s',
                                            'delay': '.3s'
                                        }">sale Up to <span class="text-primary ">30% Off</span>
                                        </h3>
                                        <a class="btn btn-white btn-icon-right" href="demo9-shop.html">Shop now <i
                                                class="d-icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="banner banner-fixed intro-slide2" style="background-color: #edecec">
                            <figure>
                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/slides/2.jpg" alt="intro-banner" width="1903"
                                    height="514" />
                            </figure>
                            <div class="container">
                                <div class="banner-content y-50">
                                    <h2 class="banner-title text-white font-weight-bold slide-animate"
                                        data-animation-options="{'name': 'fadeInUpShorter', 'duration': '1.4s', 'delay': '.4s'}">
                                        Fashion Trends</h2>
                                    <p class="font-primary slide-animate"
                                        data-animation-options="{'name': 'fadeInUpShorter', 'duration': '1.4s', 'delay': '.5s'}">
                                        Get Free Shipping on all orders over $75</p>
                                    <div class="banner-deco slide-animate"
                                        data-animation-options="{'name': 'fadeInUpShorter', 'duration': '1.4s', 'delay': '.6s'}">
                                        up to<span class="text-primary ls-normal">$100 off</span></div>
                                    <a href="demo9-shop.html" class="btn btn-white btn-icon-right slide-animate"
                                        data-animation-options="{'name': 'fadeInUp', 'duration': '1.3s', 'delay': '.8s'}">Shop
                                        now <i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="banner banner-fixed intro-slide3" style="background-color: #ed711b">
                            <figure>
                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/slides/3.jpg" alt="intro-banner" width="1903"
                                    height="514" />
                            </figure>
                            <div class="container">
                                <div class="banner-content text-center x-50 y-50">
                                    <h2 class="banner-title d-inline-block ls-normal text-white text-uppercase slide-animate font-weight-bolder"
                                        data-animation-options="{'name': 'blurIn', 'duration': '1.5s'}">Lookbook 2021
                                    </h2>
                                    <p class="mb-7 slide-animate"
                                        data-animation-options="{'name': 'fadeInUpShorter', 'duration': '1.2s', 'delay': '.5s'}">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, <br />sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="demo9-shop.html" class="btn btn-outline btn-white slide-animate"
                                        data-animation-options="{'name': 'fadeInUpShorter', 'duration': '1.2s', 'delay': '.7s'}">Go
                                        to shop</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="pt-6 mt-10">
                    <div class="container">
                        <h2 class="title title-simple">Popular Categories</h2>
                        <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-xs-2 cols-1" data-owl-options="{
                            'nav': false,
                            'dots': true,
                            'margin': 20,
                            'responsive': {
                                '0': {
                                    'items': 1
                                },
                                '480': {
                                    'items': 2
                                },
                                '768': {
                                    'items': 3
                                },
                                '992': {
                                    'items': 4,
                                    'dots': false
                                }
                            }
                        }">
                            <div class="category category-absolute category-classic">
                                <a href="demo9-shop.html">
                                    <figure class="category-media">
                                        <img src="{{ asset('ui/fronted') }}/images/demos/demo9/categories/1.jpg" alt="Cateogry" width="280"
                                            height="280" />
                                    </figure>
                                    <div class="category-content">
                                        <h4 class="category-name">Men's Collection</h4>
                                        <span class="category-count">1 Products</span>
                                    </div>
                                </a>
                            </div>
                            <div class="category category-absolute category-classic">
                                <a href="demo9-shop.html">
                                    <figure class="category-media">
                                        <img src="{{ asset('ui/fronted') }}/images/demos/demo9/categories/2.jpg" alt="Cateogry" width="280"
                                            height="280" />
                                    </figure>
                                    <div class="category-content">
                                        <h4 class="category-name">Accessories</h4>
                                        <span class="category-count">0 Products</span>
                                    </div>
                                </a>
                            </div>
                            <div class="category category-absolute category-classic">
                                <a href="demo9-shop.html">
                                    <figure class="category-media">
                                        <img src="{{ asset('ui/fronted') }}/images/demos/demo9/categories/3.jpg" alt="Cateogry" width="280"
                                            height="280" />
                                    </figure>
                                    <div class="category-content">
                                        <h4 class="category-name">For Women's</h4>
                                        <span class="category-count">0 Products</span>
                                    </div>
                                </a>
                            </div>
                            <div class="category category-absolute category-classic">
                                <a href="demo9-shop.html">
                                    <figure class="category-media">
                                        <img src="{{ asset('ui/fronted') }}/images/demos/demo9/categories/4.jpg" alt="Cateogry" width="280"
                                            height="280" />
                                    </figure>
                                    <div class="category-content">
                                        <h4 class="category-name">Top Fashional</h4>
                                        <span class="category-count">7 Products</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="products-wrapper container pt-5 mt-10 pb-4 appear-animate">
                    <div class="tab tab-nav-simple tab-nav-center">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#featured">Featured</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#new-arrivals">New Arrivals</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#best-sellers">Best Sellers</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="featured">
                                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'nav': false,
                                    'dots': false,
                                    'margin': 20,
                                    'autoplay': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '576': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        }
                                    }
                                }">
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/1.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/1-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">25% Off</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Converse Season Shoes</a>
                                            </h3>
                                            <div class="product-price">
                                                <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/2.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/2-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Man's Grey Wrist Watch</a>
                                            </h3>
                                            <div class="product-price">
                                                <span class="price">$35.00</span>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/3.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/3-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">25% Off</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Hand Electric Cell</a>
                                            </h3>
                                            <div class="product-price">
                                                <span class="price">$19.00</span>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/4.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/4-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Men's Fashion Jacket</a>
                                            </h3>
                                            <div class="product-price">
                                                <ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="new-arrivals">
                                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'nav': false,
                                    'dots': false,
                                    'margin': 20,
                                    'autoplay': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '576': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        }
                                    }
                                }">
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/1.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/1-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">25% Off</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Converse Season Shoes</a>
                                            </h3>
                                            <div class="product-price">
                                                <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/6.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/6-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Men's Winter Hood</a>
                                            </h3>
                                            <div class="product-price">
                                                <span class="price">$35.00</span>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/7.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/7-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">25% Off</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Women's Comforter</a>
                                            </h3>
                                            <div class="product-price">
                                                <span class="price">$19.00</span>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/2.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/2-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Men's Grey Wrist Watch</a>
                                            </h3>
                                            <div class="product-price">
                                                <ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="best-sellers">
                                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'nav': false,
                                    'dots': false,
                                    'margin': 20,
                                    'autoplay': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '576': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        }
                                    }
                                }">
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/4.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/4-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">25% Off</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Men's Fashion Jacket</a>
                                            </h3>
                                            <div class="product-price">
                                                <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/3.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/3-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Hand Electric Cell</a>
                                            </h3>
                                            <div class="product-price">
                                                <span class="price">$35.00</span>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/2.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/2-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">25% Off</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Man's Grey Wrist Watch</a>
                                            </h3>
                                            <div class="product-price">
                                                <span class="price">$19.00</span>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product text-center product-with-qty">
                                        <figure class="product-media">
                                            <a href="demo9-product.html">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/8.jpg" alt="product" width="300"
                                                    height="338">
                                                <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/8-1.jpg" alt="product" width="300"
                                                    height="338">
                                            </a>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                    View</a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <h3 class="product-name">
                                                <a href="demo9-product.html">Blue Comfortable Cup</a>
                                            </h3>
                                            <div class="product-price">
                                                <ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
                                            </div>
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                            </div>
                                            <div class="product-action">
                                                <div class="product-quantity">
                                                    <button class="quantity-minus d-icon-minus"
                                                        title="quantity"></button>
                                                    <input class="quantity form-control" title="quantity-input"
                                                        type="number" min="1" max="1000000">
                                                    <button class="quantity-plus d-icon-plus" title="quantity"></button>
                                                </div>
                                                <a href="#" class="btn-product btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to cart"><i
                                                        class="d-icon-bag"></i><span>Add to cart</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="sales-section container mt-10 pt-2">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 mb-6">
                            <h2 class="title title-simple text-left">On sale this week’s</h2>
                            <div class="banner banner-fixed" style="background-color: #333">
                                <figure>
                                    <img src="{{ asset('ui/fronted') }}/images/demos/demo9/banners/1.jpg" alt="banner" width="380" height="286" />
                                </figure>
                                <div class="banner-content text-center x-50 y-50 appear-animate">
                                    <h3 class="banner-title text-white font-weight-bold">Black Friday Sale</h3>
                                    <h4 class="banner-subtitle text-white text-uppercase font-weight-bold">Up TO <span
                                            class="text-primary font-weight-bolder">70% Off</span>
                                    </h4>
                                    <p class="mb-5 text-white text-uppercase">Everything</p>
                                    <a href="demo9-shop.html" class="btn btn-outline btn-white btn-md">Shop now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6 order-md-first mb-6">
                            <h2 class="title title-simple text-left mb-5">Our Sale Offer</h2>
                            <div class="owl-carousel owl-theme row owl-nav-full cols-xl-4 cols-lg-3 cols-2"
                                data-owl-options="{
                                'items': 4,
                                'nav': true,
                                'dots': false,
                                'margin': 20,
                                'loop': false,
                                'responsive': {
                                    '0': {
                                        'items': 2
                                    },
                                    '992': {
                                        'items': 3
                                    },
                                    '1200': {
                                        'items': 4
                                    }
                                }
                            }">
                                <div class="product text-center appear-animate" data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.6s'
                                }">
                                    <figure class="product-media">
                                        <a href="demo9-product.html">
                                            <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/6.jpg" alt="product" width="300"
                                                height="338">
                                        </a>
                                        <div class="product-label-group">
                                            <label class="product-label label-new">new</label>
                                        </div>
                                        <div class="product-action-vertical">
                                            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                data-target="#addCartModal" title="Add to cart"><i
                                                    class="d-icon-bag"></i></a>
                                            <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                    class="d-icon-heart"></i></a>
                                        </div>
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                View</a>
                                        </div>
                                    </figure>
                                    <div class="product-details">
                                        <h3 class="product-name">
                                            <a href="demo9-product.html">Men's Winter Hood</a>
                                        </h3>
                                        <div class="product-price">
                                            <span class="price">$184.00</span>
                                        </div>
                                        <div class="ratings-container">
                                            <div class="ratings-full">
                                                <span class="ratings" style="width:80%"></span>
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div>
                                            <a href="demo9-product.html" class="rating-reviews">( 6 reviews )</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product text-center appear-animate" data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.5s'
                                }">
                                    <figure class="product-media">
                                        <a href="demo9-product.html">
                                            <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/7.jpg" alt="product" width="300"
                                                height="338">
                                        </a>
                                        <div class="product-label-group">
                                            <label class="product-label label-sale">12% off</label>
                                        </div>
                                        <div class="product-action-vertical">
                                            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                data-target="#addCartModal" title="Add to cart"><i
                                                    class="d-icon-bag"></i></a>
                                            <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                    class="d-icon-heart"></i></a>
                                        </div>
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                View</a>
                                        </div>
                                    </figure>
                                    <div class="product-details">
                                        <h3 class="product-name">
                                            <a href="demo9-product.html">Women's Comforter</a>
                                        </h3>
                                        <div class="product-price">
                                            <ins class="new-price">$53.99</ins><del class="old-price">$67.99</del>
                                        </div>
                                        <div class="ratings-container">
                                            <div class="ratings-full">
                                                <span class="ratings" style="width:60%"></span>
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div>
                                            <a href="demo9-product.html" class="rating-reviews">( 16 reviews )</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product text-center appear-animate" data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.4s'
                                }">
                                    <figure class="product-media">
                                        <a href="demo9-product.html">
                                            <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/8.jpg" alt="product" width="300"
                                                height="338">
                                        </a>

                                        <div class="product-label-group">
                                            <label class="product-label label-sale">27% off</label>
                                        </div>
                                        <div class="product-action-vertical">
                                            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                data-target="#addCartModal" title="Add to cart"><i
                                                    class="d-icon-bag"></i></a>
                                            <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                    class="d-icon-heart"></i></a>
                                        </div>
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                View</a>
                                        </div>
                                    </figure>
                                    <div class="product-details">
                                        <h3 class="product-name">
                                            <a href="demo9-product.html">Blue Comfortable Bag</a>
                                        </h3>
                                        <div class="product-price">
                                            <ins class="new-price">$125.99</ins><del class="old-price">$160.99</del>
                                        </div>
                                        <div class="ratings-container">
                                            <div class="ratings-full">
                                                <span class="ratings" style="width:60%"></span>
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div>
                                            <a href="demo9-product.html" class="rating-reviews">( 8 reviews )</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product text-center appear-animate" data-animation-options="{
                                    'name': 'fadeInLeftShorter',
                                    'delay': '.3s'
                                }">
                                    <figure class="product-media">
                                        <a href="demo9-product.html">
                                            <img src="{{ asset('ui/fronted') }}/images/demos/demo9/products/9.jpg" alt="product" width="300"
                                                height="338">
                                        </a>
                                        <div class="product-action-vertical">
                                            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                data-target="#addCartModal" title="Add to cart"><i
                                                    class="d-icon-bag"></i></a>
                                            <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                    class="d-icon-heart"></i></a>
                                        </div>
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                                                View</a>
                                        </div>
                                    </figure>
                                    <div class="product-details">
                                        <h3 class="product-name">
                                            <a href="demo9-product.html">Leather Belt</a>
                                        </h3>
                                        <div class="product-price">
                                            <span class="price">$78.64</span>
                                        </div>
                                        <div class="ratings-container">
                                            <div class="ratings-full">
                                                <span class="ratings" style="width:40%"></span>
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div>
                                            <a href="demo9-product.html" class="rating-reviews">( 2 reviews )</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="container mt-10">
                    <div class="row">
                        <div class="col-lg-8 mb-6 appear-animate" data-animation-options="{
                            'name': 'fadeInRightShorter',
                            'delay': '.2s'
                        }">
                            <h2 class="title title-simple text-left">What we do</h2>
                            <div class="post post-list overlay-dark">
                                <figure class="post-media">
                                    <a href="post-single.html">
                                        <img src="{{ asset('ui/fronted') }}/images/demos/demo9/blog.jpg" width="380" height="350" alt="post" />
                                    </a>
                                </figure>
                                <div class="post-details text-center appear-animate">
                                    <h4 class="post-title text-dark"><a href="post-single.html">We are a Patient
                                            Experienced team with big Ambitions.</a></h4>
                                    <p class="post-content text-grey">Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmotempor dunt ut labore et doloremagna…</p>
                                    <a href="post-single.html" class="btn btn-dark btn-icon-right">Read More<i
                                            class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-6 appear-animate" data-animation-options="{
                            'name': 'fadeInLeftShorter',
                            'delay': '.2s'
                        }">
                            <h2 class="title title-simple text-left">Contact Us</h2>
                            <div class="banner banner-fixed banner-cta" style="background-color: #ed711b">
                                <figure>
                                    <img src="{{ asset('ui/fronted') }}/images/demos/demo9/banners/2.jpg" alt="banner" width="380" height="350" />
                                </figure>
                                <div class="banner-content w-100 x-50 y-50 text-center appear-animate">
                                    <h3 class="banner-title font-weight-normal text-uppercase text-white">Subscribe
                                        to our <strong>Newsletter</strong></h3>
                                    <p class="text-white">Start Shopping Right Now</p>
                                    <form action="#" class="input-wrapper">
                                        <input type="email" class="form-control text-center text-white mb-4"
                                            name="email" id="email" placeholder="Email address here..." required />
                                        <button class="btn btn-solid btn-icon-right btn-white" type="submit">subscribe<i
                                                class="d-icon-arrow-right ml-1"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="client-section mt-6 pb-4 mb-10">
                    <div class="container">
                        <h2 class="title title-simple mb-5">The Best Clients</h2>
                        <div class="owl-carousel owl-shadow-carousel owl-theme row cols-xl-6 cols-lg-5 cols-md-4 cols-sm-3 cols-2 appear-animate"
                            data-animation-options="{
                            'delay': '.3s'
                        }" data-owl-options="{
                            'items': 6,
                            'margin': 20,
                            'nav': false,
                            'dots': false,
                            'autoplay': true,
                            'responsive': {
                                '0': {
                                    'items': 2
                                },
                                '576': {
                                    'items': 3
                                },
                                '768': {
                                    'items': 4
                                },
                                '992': {
                                    'items': 5
                                },
                                '1200': {
                                    'items': 6
                                }
                            }                        
                        }">
                            <figure><img src="{{ asset('ui/fronted') }}/images/brands/1.png" alt="brand" width="180" height="100" />
                            </figure>
                            <figure><img src="{{ asset('ui/fronted') }}/images/brands/2.png" alt="brand" width="180" height="100" />
                            </figure>
                            <figure><img src="{{ asset('ui/fronted') }}/images/brands/3.png" alt="brand" width="180" height="100" />
                            </figure>
                            <figure><img src="{{ asset('ui/fronted') }}/images/brands/4.png" alt="brand" width="180" height="100" />
                            </figure>
                            <figure><img src="{{ asset('ui/fronted') }}/images/brands/5.png" alt="brand" width="180" height="100" />
                            </figure>
                            <figure><img src="{{ asset('ui/fronted') }}/images/brands/6.png" alt="brand" width="180" height="100" />
                            </figure>
                        </div>
                    </div>
                </section>
                <section class="instagram-section pb-10 appear-animate" data-animation-options="{
                    'delay': '.3s'
                }">
                    <div class="container pb-8">
                        <h2 class="title title-simple mb-5">Our Instagram</h2>
                        <div class="owl-carousel owl-theme row cols-xl-5 cols-lg-4 cols-md-3 cols-sm-2 cols-2"
                            data-owl-options="{
                                    'nav': false,
                                    'autoplay': true,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '576': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        },
                                        '1200': {
                                            'items': 6
                                        }
                                    }
                                }">
                            <figure class="instagram">
                                <a href="#"><img src="{{ asset('ui/fronted') }}/images/demos/demo9/instagram/1.jpg" alt="brand" width="180"
                                        height="180" /></a>
                            </figure>
                            <figure class="instagram">
                                <a href="#"><img src="{{ asset('ui/fronted') }}/images/demos/demo9/instagram/2.jpg" alt="brand" width="180"
                                        height="180" /></a>
                            </figure>
                            <figure class="instagram">
                                <a href="#"><img src="{{ asset('ui/fronted') }}/images/demos/demo9/instagram/3.jpg" alt="brand" width="180"
                                        height="180" /></a>
                            </figure>
                            <figure class="instagram">
                                <a href="#"><img src="{{ asset('ui/fronted') }}/images/demos/demo9/instagram/4.jpg" alt="brand" width="180"
                                        height="180" /></a>
                            </figure>
                            <figure class="instagram">
                                <a href="#"><img src="{{ asset('ui/fronted') }}/images/demos/demo9/instagram/5.jpg" alt="brand" width="180"
                                        height="180" /></a>
                            </figure>
                            <figure class="instagram">
                                <a href="#"><img src="{{ asset('ui/fronted') }}/images/demos/demo9/instagram/6.jpg" alt="brand" width="180"
                                        height="180" /></a>
                            </figure>
                        </div>
                    </div>
                </section>
            </div>

</x-fronted.layout.master>