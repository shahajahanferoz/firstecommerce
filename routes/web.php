<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',[DashBoardController::class, 'home'])->name('home');

Route::controller(ShopController::class)->group(function () {
    Route::get('/shop-classic-filter', 'classicfilter')->name('public.shop.classicfilter'); 
});

Route::get('/product',[ProductController::class, 'productdetails'])->name('admin.products.productdetails');



